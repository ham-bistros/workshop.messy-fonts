var lastSettingsString = null,
  timestamp = null;

function updateVariationSettings() {
  var settings = [];
  document.querySelectorAll('#input-variation-setting').forEach((el) => {
    settings.push(`'${el.getAttribute('name')}' ${parseInt(el.value)}`);
  });
  settingsString = settings.join(' ');
  if (settingsString !== lastSettingsString) {
    document.querySelector('.glyphs').style.fontVariationSettings = settingsString;
    lastSettingsString = settingsString;
  }
}

function draw() {
  updateVariationSettings();
}

function init() {
  let last = 0,
    interval = 1000 / 60;

  function tick(now) {
    if ((now - last) > interval) {
      draw();
      last = now;
    }
    window.requestAnimationFrame(tick);
  }

  window.requestAnimationFrame(tick);
}

init();

///////// normaliseur (tous les textarea affichent la même chose) /////////

var alors = document.getElementsByClassName('text-zone');
var paras = Array.prototype.slice.call(alors, 0);
var base = document.getElementById('base-text');

function updateParas(baseText, modif) {
  modif.forEach((item, i) => {
    item.innerHTML = baseText.value;
  });
}

base.addEventListener('keyup', updateParas(base, paras));


/////////////////// multiplicateur ///////////////////

function cloner(selector, multiple) {
  // Get the element
  var elem = document.querySelector(selector);
  var val = 100 / multiple;

  enTrop = Array.prototype.slice.call(document.getElementsByClassName('text-zone'));

  enTrop.forEach((el) => {
    el.remove();
  });

  for (let i = multiple; i >= 0; i--) {
    // Create a copy of it
    var clone = elem.cloneNode(true);
    clone.readOnly = true;

    // Update the ID and add a class
    clone.id = 't' + i;
    clone.classList.add('text-zone');
    clone.style.fontVariationSettings = '"MVMT" ' + val * i;

    // Inject it into the DOM
    elem.after(clone);
  }

  updateParas(base, paras);
};

//// slider qui permet de changer la "profondeur" des tracés topographiques
document.getElementById('input-depth').addEventListener('input', function() {
  cloner('#base-text', this.value);
});

//// slider qui permet de changer la taille de la fonte
document.getElementById('input-size').addEventListener('input', function() {
  textarea = Array.prototype.slice.call(document.getElementsByClassName('glyphs'));
  textarea.forEach(function(texte) {
    texte.style.fontSize = this.value + 'vw';
  }, this);
});


/////////////////// font selector ///////////////////
// garder la balise <style data-type="font"> sinon ça marche plus
(function() {
  var fontSelector = document.getElementById('font-selector');

  document.getElementById('font-selector')
    .addEventListener('change', function() {
      if (fontSelector.files.length > 0) {
        var reader = new FileReader();
        reader.addEventListener('load', function() {
          var fontUrl = reader.result,
            styles = document.head.querySelector('style[data-type="font"]');
          styles.innerHTML = "@font-face { font-family: 'to-preview'; src: url(" + fontUrl + "); }";
        });
        reader.readAsDataURL(fontSelector.files[0]);
      }
    });
})();
