#!/bin/bash

INPUT_FILE=$1
filename="${INPUT_FILE%.*}"
output="${2%.*}"
OUTPUT_FILE=$2

ttx $INPUT_FILE
python3 make-gvar-table.py $filename.ttx ${output}_temp.ttx
python3 modify-gvar-table.py ${output}_temp.ttx $output.ttx
ttx $output.ttx

# Remove temporary ttx files
rm $filename.ttx
rm ${output}_temp.ttx
