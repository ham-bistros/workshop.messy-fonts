from fonctions import *
# import fonctions as fonc

'''
Définition du template de fvar
'''

if __name__ == '__main__':
  import sys

  # if len(sys.argv) < 3:
    # print("Usage: make-gvar-table.py in.ttx out.ttx")

  pathIn = sys.argv[1]
  pathOut = sys.argv[2]

  with open(pathIn, 'r') as h:
    parser = et.XMLParser(remove_blank_text=True)
    ttx = et.parse(h, parser=parser)
    axisName = 'MVMT'
    minValue = 0
    maxValue = 100
    defaultValue = 20
    addGvarTable(ttx, 'MVMT', minValue, maxValue, defaultValue, 256)

    ttx.write(pathOut, pretty_print=True, doctype='<?xml version="1.0" encoding="UTF-8"?>')
