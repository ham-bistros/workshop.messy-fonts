import lxml.etree as et

fvarTemplate = """<fvar>
  <!-- Weight -->
  <Axis>
    <AxisTag>MVMT</AxisTag>
    <Flags>0x0</Flags>
    <MinValue>0.0</MinValue>
    <DefaultValue>0.0</DefaultValue>
    <MaxValue>100.0</MaxValue>
    <AxisNameID>256</AxisNameID>
  </Axis>
</fvar>"""

gvarTemplate = """<gvar>
    <version value="1"/>
    <reserved value="0"/>
</gvar>"""

"""
  Generates a fvartable based on a template with the given
  minValue, maxValue and defaultValue
"""
def makeFvarTable (tag, minValue, maxValue, defaultValue, nameID):
  table = et.fromstring(fvarTemplate, et.XMLParser(remove_blank_text=True))

  axisTag = table.find('Axis/AxisTag')
  axisTag.text = tag

  axisMinValue = table.find('Axis/MinValue')
  axisMinValue.text = '{:-.1f}'.format(minValue)

  axisMaxValue = table.find('Axis/MaxValue')
  axisMaxValue.text = '{:-.1f}'.format(maxValue)

  axisDefaultValue = table.find('Axis/DefaultValue')
  axisDefaultValue.text = '{:-.1f}'.format(defaultValue)

  axisNameID = table.find('Axis/AxisNameID')
  axisNameID.text = '{}'.format(nameID)

  return table

"""
 Return glyph with given name
"""
def findGlyph (ttx, glyphName):
  return ttx.find("glyf/TTGlyph[@name='{}']".format(glyphName))

"""
 Return glyph with given name
"""
def findAllGlyphs (ttx):
  return ttx.findall("glyf/TTGlyph")

def getGlyphContours(glyph):
    return glyph.findall("contour")

def getContourPoints (contour):
  return contour.findall("pt")

def getGlyphPoints(glyph):
    return glyph.findall("contour/pt")

def pause():
    pause = input(bcolors.WARNING+"ENTRÉE POUR CONTINUER"+bcolors.ENDC)

class bcolors:
    OKBLUE = '\033[94m'
    WARNING = '\033[93m'
    ENDC = '\033[0m'

def inBetweenPoints (ttx):
  gvar = et.fromstring(gvarTemplate, et.XMLParser(remove_blank_text=True))
  for glyph in findAllGlyphs(ttx):
    for contour in getGlyphContours(glyph):
        liste_points = getContourPoints(contour)
        for i, point in enumerate(liste_points):
            i2 = (i+1)%len(liste_points)
            pt = {'x': int(point.get('x')),
                 'y': int(point.get('y')),
                 'on': point.get('on')}
            pt_suiv = {'x': int(liste_points[i2].get('x')),
                      'y': int(liste_points[i2].get('y')),
                      'on': liste_points[i2].get('on')}
            if pt['on'] == '1' and pt_suiv['on'] == '1':
                newX = (pt['x'] + pt_suiv['x'])/2
                newY = (pt['y'] + pt_suiv['y'])/2
                # print('X =>',pt['x'],  bcolors.OKBLUE+str(newX)+bcolors.ENDC, pt_suiv['x'])
                # print('Y=>', pt['y'], bcolors.OKBLUE+str(newY)+bcolors.ENDC, pt_suiv['y'])
                # print()
                dictPoint = {'x': str(newX), 'y': str(newY), 'on': '0'}
                newPoint = et.Element('pt', dictPoint)
                point.addnext(newPoint)

def makeGvarTable (ttx, axisTag):
  gvar = et.fromstring(gvarTemplate, et.XMLParser(remove_blank_text=True))

  for glyph in findAllGlyphs(ttx):
    glyphVariations = et.SubElement(gvar, 'glyphVariations', { 'glyph': glyph.get('name') })
    glyphTuple = et.SubElement(glyphVariations, 'tuple')
    coord = et.SubElement(glyphTuple, 'coord', { 'axis': axisTag, 'value': str(1) })

    for pointNumber, point in enumerate(getGlyphPoints(glyph)):
      et.SubElement(glyphTuple, 'delta', { 'pt': str(pointNumber), 'x': str(0), 'y': str(0) })

  return gvar

def makeGvarTable (ttx, axisTag):
  gvar = et.fromstring(gvarTemplate, et.XMLParser(remove_blank_text=True))

  for glyph in findAllGlyphs(ttx):
    glyphVariations = et.SubElement(gvar, 'glyphVariations', { 'glyph': glyph.get('name') })
    glyphTuple = et.SubElement(glyphVariations, 'tuple')
    coord = et.SubElement(glyphTuple, 'coord', { 'axis': axisTag, 'value': str(1) })

    for pointNumber, point in enumerate(getGlyphPoints(glyph)):
      et.SubElement(glyphTuple, 'delta', { 'pt': str(pointNumber), 'x': str(0), 'y': str(0) })

  return gvar

def addGvarTable (ttx, axisTag, minValue, maxValue, defaultValue, axisID=256):
  if ttx.find('fvar') is not None:
    print('Font already has a table \'fvar\'')
  else:
    fvar = makeFvarTable(axisTag, minValue, maxValue, defaultValue, axisID)
    ttx.getroot().append(fvar)

  if ttx.find('gvar') is not None:
    print('Font already has a table \'gvar\'')
  else:
    gvar = makeGvarTable(ttx, axisTag)
    ttx.getroot().append(gvar)

if __name__ == '__main__':
  import sys

  if len(sys.argv) < 3:
    print("Usage: variabilise.py in.ttx out.ttx")

  pathIn = sys.argv[1]
  pathOut = sys.argv[2]

  with open(pathIn, 'r') as h:
    parser = et.XMLParser(remove_blank_text=True)
    ttx = et.parse(h, parser=parser)
    axisName = 'MVMT'
    minValue = 0
    maxValue = 100
    defaultValue = 20
    inBetweenPoints(ttx)
    addGvarTable(ttx, 'MVMT', minValue, maxValue, defaultValue, 256)

    ttx.write(pathOut, pretty_print=True, doctype='<?xml version="1.0" encoding="UTF-8"?>')
