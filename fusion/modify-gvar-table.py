from fonctions import *

if __name__ == '__main__':
  import sys

  # if len(sys.argv) < 3:
    # print("Usage: make-gvar-table.py in.ttx out.ttx")

  pathIn = sys.argv[1]
  pathOut = sys.argv[2]

  with open(pathIn, 'r') as h:
    ttx = et.parse(h, et.XMLParser(remove_blank_text=True))

    for glyphName, glyph, glyphPoints, deltas in getGvarVariations(ttx):
      for cnt, delta in enumerate(deltas):

        # More shift in later points
        setX(delta, getX(delta) + cnt * 2)
        setY(delta, getY(delta) + cnt * 2)

    ttx.write(pathOut, pretty_print=True, doctype='<?xml version="1.0" encoding="UTF-8"?>')
